package com.samuelHcb.TodoApp.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class ToDo {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String content;
    private LocalDate dateCreated = LocalDate.now();
    private LocalDate dateModified;
    private boolean isCompleted=false;
    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private TodoCategory todoCategories ;


}
