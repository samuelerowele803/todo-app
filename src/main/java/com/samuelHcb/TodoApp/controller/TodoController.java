package com.samuelHcb.TodoApp.controller;

import com.samuelHcb.TodoApp.dto.ToDoDto;
import com.samuelHcb.TodoApp.entity.ToDo;
import com.samuelHcb.TodoApp.service.TodoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/todo")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @PostMapping("/add")
    public ResponseEntity<Void> addTodo (@RequestBody ToDoDto toDo, @RequestParam("category") String category){
        todoService.addTodo(toDo,category);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @GetMapping("find-all")
    public ResponseEntity<Set<ToDo>> findAllTodo (){
       return new ResponseEntity<>( todoService.getTodoList(),HttpStatus.FOUND);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteTodoById (@PathVariable Long id){
        todoService.deleteTodo(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<ToDo> updateTodoById (@PathVariable Long id, @RequestBody ToDoDto toDo){
        return new ResponseEntity<>(todoService.updateTodo(id,toDo),HttpStatus.OK);

    }
}
