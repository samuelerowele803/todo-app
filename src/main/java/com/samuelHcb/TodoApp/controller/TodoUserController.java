package com.samuelHcb.TodoApp.controller;

import com.samuelHcb.TodoApp.entity.TodoUser;
import com.samuelHcb.TodoApp.repo.TodoUserRepo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class TodoUserController {
    private final TodoUserRepo todoUserRepo;

    public TodoUserController(TodoUserRepo todoUserRepo) {
        this.todoUserRepo = todoUserRepo;
    }
    @PostMapping("/create-user")
    public ResponseEntity<String> createUser(@RequestBody TodoUser todoUser){
        todoUserRepo.save(todoUser);
        return new ResponseEntity<>("SUCCESS", HttpStatus.CREATED);
    }
}
