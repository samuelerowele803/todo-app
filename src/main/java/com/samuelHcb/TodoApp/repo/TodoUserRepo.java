package com.samuelHcb.TodoApp.repo;

import com.samuelHcb.TodoApp.entity.TodoUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoUserRepo extends JpaRepository<TodoUser, Long> {
}