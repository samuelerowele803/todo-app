package com.samuelHcb.TodoApp.repo;

import com.samuelHcb.TodoApp.entity.TodoCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TodoCategoryRepo extends JpaRepository<TodoCategory, Long> {
    Optional<TodoCategory> findTodoCategoryByName(String category);
}