package com.samuelHcb.TodoApp.repo;

import com.samuelHcb.TodoApp.entity.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepo extends JpaRepository<ToDo,Long> {
}
