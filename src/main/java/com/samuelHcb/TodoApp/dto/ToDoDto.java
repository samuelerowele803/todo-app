package com.samuelHcb.TodoApp.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
public class ToDoDto implements Serializable {
    private final String content;
    private final LocalDate dateCreated;
}
