package com.samuelHcb.TodoApp.service;

import com.samuelHcb.TodoApp.dto.ToDoDto;
import com.samuelHcb.TodoApp.entity.ToDo;
import com.samuelHcb.TodoApp.entity.TodoCategory;
import com.samuelHcb.TodoApp.repo.TodoCategoryRepo;
import com.samuelHcb.TodoApp.repo.TodoRepo;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TodoServiceImp implements TodoService{

    private final TodoRepo todoRepo;
    private final TodoCategoryRepo todoCategoryRepo;

    public TodoServiceImp(TodoRepo todoRepo, TodoCategoryRepo todoCategoryRepo) {
        this.todoRepo = todoRepo;
        this.todoCategoryRepo = todoCategoryRepo;
    }

    @Override
    public void addTodo(final ToDoDto toDoDto, String category) {
        ToDo toDo = new ToDo();
        toDo.setContent(toDoDto.getContent());
        toDo.setDateCreated(toDoDto.getDateCreated());
        Optional<TodoCategory> todoCategory = Optional.ofNullable(todoCategoryRepo.findTodoCategoryByName(category)
                .orElseThrow(() -> new RuntimeException("ff")));
        toDo.setTodoCategories(todoCategory.get());
        todoRepo.save(toDo);

    }

    @Override
    public void deleteTodo(final Long id) {
        todoRepo.deleteById(id);

    }

    @Override
    public ToDo updateTodo(final Long id, ToDoDto toDoUpdate) {
        ToDo todo = todoRepo.findById(id).orElseThrow(()-> new RuntimeException("No to found for update with" + id));
        todo.setContent(toDoUpdate.getContent());
        todo.setDateCreated(todo.getDateCreated());
        todo.setDateModified(LocalDate.now());
       return todoRepo.save(todo);
    }

    @Override
    public Set<ToDo> getTodoList() {
        return todoRepo.findAll(Sort.by("id")).stream().collect(Collectors.toSet());
    }
}
