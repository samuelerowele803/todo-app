package com.samuelHcb.TodoApp.service;

import com.samuelHcb.TodoApp.dto.ToDoDto;
import com.samuelHcb.TodoApp.entity.ToDo;

import java.util.Set;

public interface TodoService {
    void addTodo(ToDoDto toDoDto, String category);
    void deleteTodo (Long id);
    ToDo updateTodo (Long id, ToDoDto toDoDto);
    Set<ToDo> getTodoList ();

}
